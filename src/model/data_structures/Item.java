package model.data_structures;

import java.awt.Point;

public class Item<E extends Comparable<E>> {

	private String object_id;

	private String row_;

	private String location;

	private String address_id;

	private String streetsegid;

	private double xcoord;

	private double ycoord;

	private String tickettype;

	private String fineamt;

	private String totalpaid;

	private String penalty1;

	private String penalty2;

	private String accidentindicator;

	private String agencyid;

	private String ticketissuedate;

	private String violationcode;

	private String violationdesc;

	private String row_id;

	public String getObject_id() {
		return object_id;
	}

	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}

	public String getRow_() {
		return row_;
	}

	public void setRow_(String row_) {
		this.row_ = row_;
	}

	public String getAddress_id() {
		return address_id;
	}

	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}

	public String getStreetsegid() {
		return streetsegid;
	}

	public void setStreetsegid(String streetsegid) {
		this.streetsegid = streetsegid;
	}

	public double getXcoord() {
		return xcoord;
	}

	public void setXcoord(double xcoord) {
		this.xcoord = xcoord;
	}

	public double getYcoord() {
		return ycoord;
	}

	public void setYcoord(double ycoord) {
		this.ycoord = ycoord;
	}

	public String getTickettype() {
		return tickettype;
	}

	public void setTickettype(String tickettype) {
		this.tickettype = tickettype;
	}

	public String getFineamt() {
		return fineamt;
	}

	public void setFineamt(String fineamt) {
		this.fineamt = fineamt;
	}

	public String getTotalpaid() {
		return totalpaid;
	}

	public void setTotalpaid(String totalpaid) {
		this.totalpaid = totalpaid;
	}

	public String getPenalty1() {
		return penalty1;
	}

	public void setPenalty1(String penalty1) {
		this.penalty1 = penalty1;
	}

	public String getPenalty2() {
		return penalty2;
	}

	public void setPenalty2(String penalty2) {
		this.penalty2 = penalty2;
	}

	public String getAccidentindicator() {
		return accidentindicator;
	}

	public void setAccidentindicator(String accidentindicator) {
		this.accidentindicator = accidentindicator;
	}

	public String getAgencyid() {
		return agencyid;
	}

	public void setAgencyid(String agencyid) {
		this.agencyid = agencyid;
	}

	public String getTicketissuedate() {
		return ticketissuedate;
	}

	public void setTicketissuedate(String ticketissuedate) {
		this.ticketissuedate = ticketissuedate;
	}

	public String getViolationcode() {
		return violationcode;
	}

	public void setViolationcode(String violationcode) {
		this.violationcode = violationcode;
	}

	public String getViolationdesc() {
		return violationdesc;
	}

	public void setViolationdesc(String violationdesc) {
		this.violationdesc = violationdesc;
	}

	public String getRow_id() {
		return row_id;
	}

	public void setRow_id(String row_id) {
		this.row_id = row_id;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getLocation() {
		return 1;
	}

}