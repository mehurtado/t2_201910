package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.DoubleLinkedList;

public class MovingViolationsManager implements IMovingViolationsManager {

	public DoubleLinkedList<VOMovingViolations> accidentList = new DoubleLinkedList<>();

	public void loadInfractions(String movingViolationsFile) {

		try {

			ArrayList<String> accidentes = new ArrayList<String>();
			CSVReader archivo = new CSVReader(movingViolationsFile);
			archivo.readRecord();
			archivo.readHeaders();
			archivo.skipLine();
			while (!archivo.skipLine()) {

				accidentes.add(archivo.getRawRecord());
				archivo.skipLine();
			}

			for (int i = 0; i < accidentes.size(); i++) {
				VOMovingViolations nuevo = new VOMovingViolations();
				String[] valores = accidentes.get(i).split(",");
				nuevo.setObject_id(Integer.parseInt(valores[0]));
				nuevo.setRow_(valores[1]);
				nuevo.setLocation(valores[2]);
				nuevo.setAddress_id(valores[3]);
				nuevo.setStreetsegid(valores[4]);
				nuevo.setXcoord(Double.parseDouble(valores[5]));
				nuevo.setYcoord(Double.parseDouble(valores[6]));
				nuevo.setTickettype(valores[7]);
				nuevo.setFineamt(valores[8]);
				nuevo.setTotalpaid(valores[9]);
				nuevo.setPenalty1(valores[10]);
				nuevo.setPenalty2(valores[11]);
				nuevo.setAccidentindicator(valores[12]);
				nuevo.setTicketissuedate(valores[13]);
				nuevo.setViolationcode(valores[14]);
				nuevo.setViolationdesc(valores[15]);
				nuevo.setRow_id(valores[16]);
				accidentList.add(nuevo);
				archivo.skipLine();

			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public DoubleLinkedList<VOMovingViolations> getMovingViolationsByViolationCode(String violationCode) {
		DoubleLinkedList<VOMovingViolations> rta = new DoubleLinkedList<>();

		for (VOMovingViolations current : accidentList) {
			if (current.getViolationcode() != null && current.getViolationcode().equals(violationCode)) {
				rta.add(current);
			}
		}
		return rta;
	}

	@Override
	public DoubleLinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOMovingViolations> rta = new DoubleLinkedList<>();

		for (VOMovingViolations current : accidentList) {
			if (current.getViolationcode() != null && current.getAccidentindicator().equals(accidentIndicator)) {
				rta.add(current);
			}
		}
		return rta;
	}

}
