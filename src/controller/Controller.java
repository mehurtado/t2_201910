package controller;

import api.IMovingViolationsManager;
import model.data_structures.DoubleLinkedList;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class Controller {

	/**
	 * Reference to the services manager
	 */

	private static IMovingViolationsManager manager = new MovingViolationsManager();

	public static void loadMovingViolations() {

		String infractionsFile = "./data/Moving_Violations_Issued_in_January_2018.csv";
		manager.loadInfractions(infractionsFile);
	}

	public static DoubleLinkedList<VOMovingViolations> getMovingViolationsByViolationCode(String violationCode) {
		return manager.getMovingViolationsByViolationCode(violationCode);
	}

	public static DoubleLinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		return manager.getMovingViolationsByAccident(accidentIndicator);
	}
}
