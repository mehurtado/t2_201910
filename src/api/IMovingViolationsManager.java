package api;

import model.data_structures.DoubleLinkedList;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 */
	
	
	public DoubleLinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode);
	
	
	public DoubleLinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator);

	void loadInfractions(String infractionsFile);

	
}
